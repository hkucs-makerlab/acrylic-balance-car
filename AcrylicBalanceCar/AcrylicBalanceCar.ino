/*
    original for  https://youtu.be/ewYFrtir-Co
    modified to use mpu-9250 imu
*/

#include <Wire.h>
#include <EEPROM.h>
#include "Kalman.h"
#include "ComPacket.h"
#include "I2C.h"
#include "MyEEprom.h"
#include "PinChangeInt.h" // for RC reciver


//#define RESTRICT_PITCH // Comment out to restrict roll to ±90deg instead - please read: http://www.freescale.com/files/sensors/doc/app_note/AN3461.pdf
//#define USEEEPROM     //uncomment to use eeprom for storing PID parameters

//motor  define
#define MOTO_RDY 2
//M2
#define DIR_R1 3
#define DIR_R2 4
#define PWM_R 5
//M1
#define PWM_L 6
#define DIR_L2 7
#define DIR_L1 8

//encoder define
//M1
#define SPD_INT_L 9
#define SPD_PUL_L 10
//M2
#define SPD_PUL_R 11
#define SPD_INT_R 12


//rc receiver   //2 channels
#define AUX_IN_PIN 14  //is it use RC ,high level :no
#define UP_DOWN_IN_PIN   16
#define LEFT_RIGHT_IN_PIN  17
#define UP_DOWN_FLAG   0b10
#define LEFT_RIGHT_FLAG 0b100

volatile uint8_t bUpdateFlagsRC = 0;
bool RCWork = false;
uint32_t UpDownStart;
uint32_t LeftRightStart;
volatile uint16_t UpDownEnd = 0;
volatile uint16_t LeftRightEnd = 0;
int UpDownIn;
int LeftRightIn;

Kalman kalmanX; // Create the Kalman instances
Kalman kalmanY;
ComPacket SerialPacket;

/* IMU Data */
double accX, accY, accZ;
double gyroX, gyroY, gyroZ;
int16_t tempRaw;

double gyroXangle, gyroYangle; // Angle calculate using the gyro only
double compAngleX, compAngleY; // Calculated angle using a complementary filter
double kalAngleX, kalAngleY; // Calculated angle using a Kalman filter

uint32_t timer;
uint8_t i2cData[14]; // Buffer for I2C data

int Speed_L, Speed_R;
int Position_AVG;
int Position_AVG_Filter;
int Position_Add;
int pwm, pwm_l, pwm_r;
double Angle_Car;
double Gyro_Car;

double KA_P, KA_D;
double KP_P, KP_I;
double K_Base;
struct EEpromData SavingData;
struct EEpromData ReadingData;

int Speed_Need, Turn_Need;
int Speed_Diff, Speed_Diff_ALL;

void setup() {
  Serial.begin(115200);
  Wire.begin();
  Init();

  // Set accelerometers low pass filter at 5Hz
  i2cWrite(MPU9250_ADDRESS, 29, 0x06, true);
  // Set gyroscope low pass filter at 5Hz
  i2cWrite(MPU9250_ADDRESS, 26, 0x06, true);
  // Configure gyroscope range
  i2cWrite(MPU9250_ADDRESS, 27, GYRO_FULL_SCALE_1000_DPS, true);
  // Configure accelerometers range
  i2cWrite(MPU9250_ADDRESS, 28, ACC_FULL_SCALE_4_G, true);
  // Set by pass mode for the magnetometers
  //i2cWrite(MPU9250_ADDRESS,0x37, 0x02, true);
  // Request continuous magnetometer measurements in 16 bits
  //i2cWrite(MPU9250_ADDRESS,0x0A, 0x16, true);

  /* Set kalman and gyro starting angle */
  while (i2cRead(MPU9250_ADDRESS, 0x3B, i2cData, 6));
  accX = (i2cData[0] << 8) | i2cData[1];
  accY = (i2cData[2] << 8) | i2cData[3];
  accZ = (i2cData[4] << 8) | i2cData[5];

  // Source: http://www.freescale.com/files/sensors/doc/app_note/AN3461.pdf eq. 25 and eq. 26
  // atan2 outputs the value of -π to π (radians) - see http://en.wikipedia.org/wiki/Atan2
  // It is then converted from radians to degrees
#ifdef RESTRICT_PITCH // Eq. 25 and 26
  double roll  = atan2(accY, accZ) * RAD_TO_DEG;
  double pitch = atan(-accX / sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;
#else // Eq. 28 and 29
  double roll  = atan(accY / sqrt(accX * accX + accZ * accZ)) * RAD_TO_DEG;
  double pitch = atan2(-accX, accZ) * RAD_TO_DEG;
#endif

  kalmanX.setAngle(roll); // Set starting angle
  kalmanY.setAngle(pitch);
  gyroXangle = roll;
  gyroYangle = pitch;
  compAngleX = roll;
  compAngleY = pitch;

  /*
    if( analogRead(AUX_IN_PIN) > 1000)
    {
     RCWork = true;
    }
    else
    {
      RCWork = false;
    }
  */
  // using the PinChangeInt library, attach the interrupts
  // used to read the channels
  //Serial.print("RCWork: "); Serial.println(RCWork);
  if (RCWork == true)
  {
    PCintPort::attachInterrupt(UP_DOWN_IN_PIN, calcUpDown, CHANGE);
    PCintPort::attachInterrupt(LEFT_RIGHT_IN_PIN, calcLeftRight, CHANGE);
  }
  PCintPort::attachInterrupt(SPD_INT_L, Encoder_L, FALLING);
  PCintPort::attachInterrupt(SPD_INT_R, Encoder_R, FALLING);

  timer = micros();
}

void loop() {

  double DataAvg[3];
  double AngleAvg = 0;

  DataAvg[0] = 0; DataAvg[1] = 0; DataAvg[2] = 0;

  while (1)
  { //Serial.println("here");
    if (UpdateAttitude())
    {

      DataAvg[2] = DataAvg[1];
      DataAvg[1] = DataAvg[0];
      DataAvg[0] = Angle_Car;
      AngleAvg = (DataAvg[0] + DataAvg[1] + DataAvg[2]) / 3;
      if (AngleAvg < 40 || AngleAvg > -40) {
        PWM_Calculate();
        Car_Control();
      }
    }
    UserComunication();
  }


}

bool StopFlag = true;

void PWM_Calculate()
{

  float ftmp = 0;
  ftmp = (Speed_L + Speed_R) * 0.5;
  if ( ftmp > 0)
    Position_AVG = ftmp + 0.5;
  else
    Position_AVG = ftmp - 0.5;

  Speed_Diff = Speed_L - Speed_R;
  Speed_Diff_ALL += Speed_Diff;

  //  Position_AVG_Filter *= 0.5;		//speed filter
  //  Position_AVG_Filter += Position_AVG * 0.5;
  Position_AVG_Filter = Position_AVG;

  Position_Add += Position_AVG_Filter;  //position
  //Serial.print(Speed_Need);  Serial.print("\t"); Serial.println(Turn_Need);

  Position_Add += Speed_Need;  //

  Position_Add = constrain(Position_Add, -800, 800);
  //   Serial.print(Position_AVG_Filter);  Serial.print("\t"); Serial.println(Position_Add);
  //Serial.print((Angle_Car-2 + K_Base)* KA_P);  Serial.print("\t");
  pwm =  (Angle_Car + K_Base) * KA_P //P
         + Gyro_Car * KA_D //D
         +  Position_Add * KP_I    //I
         +  Position_AVG_Filter * KP_P; //P
  // Serial.println(pwm);

  // if(Speed_Need ==0)
  //   StopFlag = true;

  if ((Speed_Need != 0) && (Turn_Need == 0)) {
    if (StopFlag == true)
    {
      Speed_Diff_ALL = 0;
      StopFlag = false;
    }
    pwm_r = int(pwm + Speed_Diff_ALL);
    pwm_l = int(pwm - Speed_Diff_ALL);

  } else {
    StopFlag = true;
    pwm_r = pwm + Turn_Need; //
    pwm_l = pwm - Turn_Need;

  }

  Speed_L = 0;
  Speed_R = 0;
}

void Car_Control()
{


  if (pwm_l < 0)
  {
    digitalWrite(DIR_L1, HIGH);
    digitalWrite(DIR_L2, LOW);
    pwm_l = - pwm_l; //cchange to positive
  }
  else
  {
    digitalWrite(DIR_L1, LOW);
    digitalWrite(DIR_L2, HIGH);
  }

  if (pwm_r < 0)
  {
    digitalWrite(DIR_R1, LOW);
    digitalWrite(DIR_R2, HIGH);
    pwm_r = -pwm_r;
  }
  else
  {
    digitalWrite(DIR_R1, HIGH);
    digitalWrite(DIR_R2, LOW);
  }
  if ( Angle_Car > 45 || Angle_Car < -45 )
  {
    pwm_l = 0;
    pwm_r = 0;
  }
  
  // pwm_l = pwm_l;  //adjust Motor different
  //  pwm_r = pwm_r;
  // Serial.print(pwm_l);  Serial.print("\t"); Serial.println(pwm_r);

/*
  digitalWrite(DIR_L1, HIGH);
  digitalWrite(DIR_L2, LOW);
  digitalWrite(DIR_R1, LOW);
  digitalWrite(DIR_R2, HIGH);
  analogWrite(PWM_L, 255);
  */
  //analogWrite(PWM_R, 255);

  
  analogWrite(PWM_L, pwm_l > 255 ? 255 : pwm_l);
  analogWrite(PWM_R, pwm_r > 255 ? 255 : pwm_r);


  /*
    Serial.print("pwm: "); Serial.println(pwm);
    Serial.print("pwm_L: "); Serial.println(pwm_l);
    Serial.print("pwm_R: "); Serial.println(pwm_r);
  */
}


void UserComunication()
{
  MySerialEvent();
  if (SerialPacket.m_PackageOK == true)
  {
    SerialPacket.m_PackageOK = false;
    switch (SerialPacket.m_Buffer[4])
    {
      case 0x01: break;
      case 0x02: UpdatePID(); break;
      case 0x03: CarDirection(); break;
      case 0x04: SendPID(); break;
      case 0x05:
        SavingData.KA_P = KA_P;
        SavingData.KA_D = KA_D;
        SavingData.KP_P = KP_P;
        SavingData.KP_I = KP_I;
        SavingData.K_Base = K_Base;
        WritePIDintoEEPROM(&SavingData);
        break;
      case 0x06:  break;
      case 0x07: break;
      default: break;
    }
  }

}

void UpdatePID()
{
  unsigned int Upper, Lower;
  double NewPara;
  Upper = SerialPacket.m_Buffer[2];
  Lower = SerialPacket.m_Buffer[1];
  NewPara = (float)(Upper << 8 | Lower) / 100.0;
  switch (SerialPacket.m_Buffer[3])
  {
    case 0x01: KA_P = NewPara; Serial.print("Get KA_P: \n"); Serial.println(KA_P); break;
    case 0x02: KA_D = NewPara; Serial.print("Get KA_D: \n"); Serial.println(KA_D); break;
    case 0x03: KP_P = NewPara; Serial.print("Get KP_P: \n"); Serial.println(KP_P); break;
    case 0x04: KP_I = NewPara; Serial.print("Get KP_I: \n"); Serial.println(KP_I); break;
    case 0x05: K_Base = NewPara; Serial.print("Get K_Base: \n"); Serial.println(K_Base); break;
    default: break;
  }
}

void CarDirection()
{
  unsigned char Speed = SerialPacket.m_Buffer[1];
  switch (SerialPacket.m_Buffer[3])
  {
    case 0x00: Speed_Need = 0; Turn_Need = 0; break;
    case 0x01: Speed_Need = -Speed; break;
    case 0x02: Speed_Need = Speed; break;
    case 0x03: Turn_Need = Speed; break;
    case 0x04: Turn_Need = -Speed; break;
    default: break;
  }
}

void SendPID()
{
  static unsigned char cnt = 0;
  unsigned char data[3];
  switch (cnt)
  {
    case 0:
      data[0] = 0x01;
      data[1] = int(KA_P * 100) / 256;
      data[2] = int(KA_P * 100) % 256;
      AssemblyAndSend(0x04, data);
      cnt++; break;
    case 1:
      data[0] = 0x02;
      data[1] = int(KA_D * 100) / 256;
      data[2] = int(KA_D * 100) % 256;
      AssemblyAndSend(0x04, data);
      cnt++; break;
    case 2:
      data[0] = 0x03;
      data[1] = int(KP_P * 100) / 256;
      data[2] = int(KP_P * 100) % 256;
      AssemblyAndSend(0x04, data);
      cnt++; break;
    case 3:
      data[0] = 0x04;
      data[1] = int(KP_I * 100) / 256;
      data[2] = int(KP_I * 100) % 256;
      AssemblyAndSend(0x04, data);
      cnt++; break;
    case 4:
      data[0] = 0x05;
      data[1] = int(K_Base * 100) / 256;
      data[2] = int(K_Base * 100) % 256;
      AssemblyAndSend(0x04, data);
      cnt = 0; break;
    default: break;
  }
}

void AssemblyAndSend(char type , unsigned char *data)
{
  unsigned char sendbuff[6];
  sendbuff[0] = 0xAA;
  sendbuff[1] = type;
  sendbuff[2] = data[0];
  sendbuff[3] = data[1];
  sendbuff[4] = data[2];
  sendbuff[5] = data[0] ^ data[1] ^ data[2];
  for (int i = 0; i < 6; i++)
  {
    Serial.write(sendbuff[i]);
  }
}

void Init()
{

  pinMode(MOTO_RDY, OUTPUT);
  digitalWrite(MOTO_RDY, HIGH);
  pinMode(SPD_PUL_L, INPUT);
  pinMode(SPD_INT_L, INPUT);  
  pinMode(SPD_PUL_R, INPUT);
  pinMode(SPD_INT_R, INPUT);
  pinMode(PWM_L, OUTPUT);
  pinMode(PWM_R, OUTPUT);
  pinMode(DIR_L1, OUTPUT);
  pinMode(DIR_L2, OUTPUT);
  pinMode(DIR_R1, OUTPUT);
  pinMode(DIR_R2, OUTPUT);

  pinMode(AUX_IN_PIN, INPUT);
  pinMode(UP_DOWN_IN_PIN, INPUT);
  pinMode(LEFT_RIGHT_IN_PIN, INPUT);

  // check imu
  uint8_t whoami = -1;
  i2cRead(MPU9250_ADDRESS, 0x75, &whoami, 1);
  if (whoami != 0x71) {
    Serial.println(F("Error, mpu-9250 not found, program halts!"));
    while (true);
  } else {
    Serial.println(F("mpu-9250 found."));
  }

  //init variables
  Speed_L = 0;
  Speed_R = 0;
  Position_AVG = 0;
  Position_AVG_Filter = 0;
  Position_Add = 0;
  pwm = 0; pwm_l = 0; pwm_r = 0;
  Speed_Diff = 0; Speed_Diff_ALL = 0;

  KA_P = 100.0;//300.0;//25
  KA_D = 0;//30;
  KP_P = 0;//30;
  KP_I = 0;//0.34;
  K_Base = -4;

  ReadingData.KA_P = KA_P;
  ReadingData.KA_D = KA_D;
  ReadingData.KP_P = KP_P;
  ReadingData.KP_I = KP_I;
  ReadingData.K_Base = K_Base;

  Speed_Need = 0;
  Turn_Need = 0;
  ReadFromEEprom(&ReadingData);
  KA_P = ReadingData.KA_P;
  KA_D = ReadingData.KA_D;
  KP_P = ReadingData.KP_P;
  KP_I = ReadingData.KP_I;
  K_Base = ReadingData.K_Base;
  Serial.print("PID Data is"); Serial.print("\t");
  Serial.print(KA_P); Serial.print("\t");
  Serial.print(KA_D); Serial.print("\t");
  Serial.print(KP_P); Serial.print("\t");
  Serial.print(KP_I); Serial.print("\t");
  Serial.print(K_Base); Serial.println("\t");
}


void Encoder_L()   //car up is positive car down  is negative
{
  if (digitalRead(SPD_PUL_L))
    Speed_L += 1;
  else
    Speed_L -= 1;
  //  Serial.print("SPEED_L:    ");
  // Serial.println(Speed_L);
}

void Encoder_R()    //car up is positive car down  is negative
{
  if (!digitalRead(SPD_PUL_R))
    Speed_R += 1;
  else
    Speed_R -= 1;

  // Serial.print("SPEED_R:    ");
  // Serial.println(Speed_R);
}


void MySerialEvent()
{
  uchar c = '0';
  uchar tmp = 0;
  if (Serial.available()) {
    c = (uchar)Serial.read();
    //Serial.println("here");
    for (int i = 5; i > 0; i--)
    {
      SerialPacket.m_Buffer[i] = SerialPacket.m_Buffer[i - 1];
    }
    SerialPacket.m_Buffer[0] = c;

    if (SerialPacket.m_Buffer[5] == 0xAA)
    {
      tmp = SerialPacket.m_Buffer[1] ^ SerialPacket.m_Buffer[2] ^ SerialPacket.m_Buffer[3];
      if (tmp == SerialPacket.m_Buffer[0])
      {
        SerialPacket.m_PackageOK = true;
      }
    }
  }
}

int UpdateAttitude()
{
  if ((micros() - timer) >= 10000)
  { //10ms
    /* Update all the values */
    while (i2cRead(MPU9250_ADDRESS, 0x3B, i2cData, 14));
    accX = ((i2cData[0] << 8) | i2cData[1]);
    accY = ((i2cData[2] << 8) | i2cData[3]);
    accZ = ((i2cData[4] << 8) | i2cData[5]);
    tempRaw = (i2cData[6] << 8) | i2cData[7];
    gyroX = (i2cData[8] << 8) | i2cData[9];
    gyroY = (i2cData[10] << 8) | i2cData[11];
    gyroZ = (i2cData[12] << 8) | i2cData[13];

    double dt = (double)(micros() - timer) / 1000000; // Calculate delta time
    timer = micros();

    // Source: http://www.freescale.com/files/sensors/doc/app_note/AN3461.pdf eq. 25 and eq. 26
    // atan2 outputs the value of -π to π (radians) - see http://en.wikipedia.org/wiki/Atan2
    // It is then converted from radians to degrees
#ifdef RESTRICT_PITCH // Eq. 25 and 26
    double roll  = atan2(accY, accZ) * RAD_TO_DEG;
    double pitch = atan(-accX / sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;
#else // Eq. 28 and 29
    double roll  = atan(accY / sqrt(accX * accX + accZ * accZ)) * RAD_TO_DEG;
    double pitch = atan2(-accX, accZ) * RAD_TO_DEG;
#endif

    double gyroXrate = gyroX / 131.0; // Convert to deg/s
    double gyroYrate = gyroY / 131.0; // Convert to deg/s

#ifdef RESTRICT_PITCH
    // This fixes the transition problem when the accelerometer angle jumps between -180 and 180 degrees
    if ((roll < -90 && kalAngleX > 90) || (roll > 90 && kalAngleX < -90)) {
      kalmanX.setAngle(roll);
      compAngleX = roll;
      kalAngleX = roll;
      gyroXangle = roll;
    } else
      kalAngleX = kalmanX.getAngle(roll, gyroXrate, dt); // Calculate the angle using a Kalman filter

    if (abs(kalAngleX) > 90)
      gyroYrate = -gyroYrate; // Invert rate, so it fits the restriced accelerometer reading
    kalAngleY = kalmanY.getAngle(pitch, gyroYrate, dt);
#else
    // This fixes the transition problem when the accelerometer angle jumps between -180 and 180 degrees
    if ((pitch < -90 && kalAngleY > 90) || (pitch > 90 && kalAngleY < -90)) {
      kalmanY.setAngle(pitch);
      compAngleY = pitch;
      kalAngleY = pitch;
      gyroYangle = pitch;
    } else
      kalAngleY = kalmanY.getAngle(pitch, gyroYrate, dt); // Calculate the angle using a Kalman filter

    if (abs(kalAngleY) > 90)
      gyroXrate = -gyroXrate; // Invert rate, so it fits the restriced accelerometer reading
    kalAngleX = kalmanX.getAngle(roll, gyroXrate, dt); // Calculate the angle using a Kalman filter
#endif

    // gyroXangle += gyroXrate * dt; // Calculate gyro angle without any filter
    // gyroYangle += gyroYrate * dt;
    //gyroXangle += kalmanX.getRate() * dt; // Calculate gyro angle using the unbiased rate
    //gyroYangle += kalmanY.getRate() * dt;

    // compAngleX = 0.93 * (compAngleX + gyroXrate * dt) + 0.07 * roll; // Calculate the angle using a Complimentary filter
    //compAngleY = 0.93 * (compAngleY + gyroYrate * dt) + 0.07 * pitch;

    // Reset the gyro angle when it has drifted too much
    if (gyroXangle < -180 || gyroXangle > 180)
      gyroXangle = kalAngleX;
    if (gyroYangle < -180 || gyroYangle > 180)
      gyroYangle = kalAngleY;

    /* Print Data */
#if 0 // Set to 1 to activate
    Serial.print(accX); Serial.print("\t");
    Serial.print(accY); Serial.print("\t");
    Serial.print(accZ); Serial.print("\t");

    Serial.print(gyroX); Serial.print("\t");
    Serial.print(gyroY); Serial.print("\t");
    Serial.print(gyroZ); Serial.print("\t");

    Serial.print("\t");
#endif

#if 0
    Serial.print("Roll: "); Serial.print(roll);
    // Serial.print(gyroXangle); Serial.print("\t");
    // Serial.print(compAngleX); Serial.print("\t");
    Serial.print(" Kal roll: ");Serial.println(kalAngleX); 


    Serial.print("Pitch: "); Serial.print(pitch); 
    // Serial.print(gyroYangle); Serial.print("\t");
    // Serial.print(compAngleY); Serial.print("\t");
    Serial.print(" Kal pitch: "); Serial.println(kalAngleY);
#endif
#if 0 // Set to 1 to print the temperature
    Serial.print("\t");

    double temperature = (double)tempRaw / 340.0 + 36.53;
    Serial.print(temperature); Serial.print("\t");
#endif

    //Serial.print("\r\n");

    Angle_Car = -kalAngleY;   //negative backward  positive forward
    Gyro_Car = -gyroYrate;
    //Serial.print(Angle_Car);Serial.print("\t");Serial.println(Gyro_Car);

    return 1;
  }
  return 0;
}

void magnetometer()
{
  // _____________________
  // :::  Magnetometer :::
  // Read register Status 1 and wait for the DRDY: Data Ready
  uint8_t ST1;
  do
  {
    i2cRead(MAG_ADDRESS, 0x02, &ST1, 1);
  }
  while (!(ST1 & 0x01));

  // Read magnetometer data
  uint8_t Mag[7];
  i2cRead(MAG_ADDRESS, 0x03, Mag, 7);

  // Create 16 bits values from 8 bits data

  // Magnetometer
  int16_t mx = -(Mag[3] << 8 | Mag[2]);
  int16_t my = -(Mag[1] << 8 | Mag[0]);
  int16_t mz = -(Mag[5] << 8 | Mag[4]);

  // Magnetometer
  Serial.print (mx + 200, DEC);
  Serial.print ("\t");
  Serial.print (my - 70, DEC);
  Serial.print ("\t");
  Serial.print (mz - 700, DEC);
  Serial.print ("\t");

  // End of line
  Serial.println();
}

//rc receiver interrupt routine
//------------------------------------------------------
void calcUpDown()
{
  // Serial.println("in up down here");
  if (digitalRead(UP_DOWN_IN_PIN) == HIGH)
  {
    UpDownStart = micros();
  }
  else
  {
    UpDownEnd = (uint16_t)(micros() - UpDownStart);
    bUpdateFlagsRC |= UP_DOWN_FLAG;
  }
}

void calcLeftRight()
{
  // Serial.println("in Left Right here");
  if (digitalRead(LEFT_RIGHT_IN_PIN) == HIGH)
  {
    LeftRightStart = micros();
  }
  else
  {
    LeftRightEnd = (uint16_t)(micros() - LeftRightStart);
    bUpdateFlagsRC |= LEFT_RIGHT_FLAG;
  }
}

